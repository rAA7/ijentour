from guest import Guest
from price import Price
from platform import system as sysname
from os import system as sys

"""
1. Guest
2. Select 
---Homestay
---Transport
---Ticket
3. Calculation Price
4. Operation Price
5. Ouput : Total All Price
======END=====
"""
price = Price()
user = {
    'car_select' : [],
}
cls = lambda: sys('cls') if sysname().lower() =='windows' else sys('clear')


def facilities(**kwargs):
    return {
        1 : selectCar(),
        2 : 'Homestay',
        3 : 'Ticket',
    }[kwargs['select']]

def selectCar():
    select_list = {
        1:'avansa',
        2:'troper'
    }
    car_list = price.transport['car']
    d = user['guest']['total']

    while d>0:
        cls()
        n = 1
        if len(user['car_select']) > 0:
            print "Pilih Mobil Lagi.."

        for k,v in car_list.iteritems():
            print "{n}. {k}".format(k=k,n=n)
            n+=1
        i = input("Pilih Mobil :")
        user['car_select'].append(select_list[i])
        d-= car_list.get(select_list[i])['max']
        user.update({'car' : car_list.get(select_list[i])})
    print user['car_select']

def main():
    inpt = {
        'inter' : input("Guest Inter : "),
        'local' : input("Guest Local : ")
    }
    g = Guest(inpt)
    user.update({'guest': inpt})
    user['guest'].update({'total' : sum(user['guest'].values())})
    print "Total Guest: {guest}".format(guest=g.totalguest)
    print g
    d=0
    while d < 5:
        cls()
        print """
        1. Transport
        2. Homestay
        3. Ticket
        4. Total
        5. Exit
        """

        d = input("Select Service [number]: ")
        if d < 4:
            print facilities(select=d)

if __name__=='__main__':
    main()
