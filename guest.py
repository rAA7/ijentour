from abc import ABCMeta, abstractmethod

"""
App TourIjen
Guest Class Interface 
@author ri7nz
study case : local guest & manca guest

"""
class iGuest(object):
    __metaclass__ = ABCMeta
    @abstractmethod
    def setGuest(self, guest):
        pass
class Guest(iGuest):
    def __init__(self,guest):
       self.__guest=None
       self.setGuest(guest)

    def setGuest(self, guest):
        """
        format guest{
            'local' : 2,
            'inter' : 3
        }
        """
        #guest = Struct(**guest)
        for k,v in guest.iteritems():
            setattr(self,k+'Guest',int(v))
        setattr(self,'totalguest', sum(guest.values()))
        self.__guest=guest
    def __repr__(self):
        text = """=================\n=     Guest     =\n=================\nLocal : {local} people\nInter : {inter} people"""
        if self.__guest is None:
            return text.format(local=0, inter=0)
        else:
            return text.format(local= self.__guest['local'], inter=self.__guest['inter'])
