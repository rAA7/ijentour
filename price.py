from abc import ABCMeta, abstractmethod

class iPrice(object):
    __metaclass__ = ABCMeta

price = {
    'transport' : {
        'car' : {
            'troper' : {
        'name' : 'Mobil Troper',
        'price' : 200000,
        'driver' : 100000,
        'bensin' : 100000,
        'max' : 15,
        },
            'avansa' : {
        'name' : 'Avansa',
        'price' : 100000,
        'driver' : 100000,
        'bensin' : 100000,
        'max' : 5,
        }
        }
    },
    'homestay' : {
        'name' : 'Homestay',
        'price' : 100000,
    },
    'ticket' : {
        'weekend' : {
            'name' : 'Ticket Weekend',
            'price' : {
                'local' : 7500,
                'inter' : 150000
            }
        },
        'normal' : {
            'name' : 'Ticket Normal',
            'price' : {
                'local' : 5000,
                'inter' : 100000
            }
        }
    },
    'tourguide' : {
        'name': 'Tour Guide',
        'price': 300000,
    }
}
class Price(iPrice):
    def __init__(self):
        for k,v in price.iteritems(): 
            setattr(self, k, v)
    def getTransport(self):
        transport = self.list_price['transport']
        #transport.update({ 'total' : self.sumTotal(transport)})
        tourguide = self.list_price['tourguide']
        #tourguide.update({ 'total' : self.sumTotal(tourguide)})

        print "Transport"
        self.printObj(transport)
        print "Tour Guide"
        self.printObj(tourguide)
        total = self.sumTotal(transport) + self.sumTotal(tourguide)
        print "Total Transport Price: {total}".format(total=total)

    def sumTotal(self, value):
        if isinstance(value, (dict)):
            return sum({ k:v for k,v in value.iteritems() if isinstance(v, (int,float))}.values())
        return None

    def printObj(self, obj):
        text = "{k} : {v}"
        if isinstance(obj,dict):
            for k,v in obj.iteritems():
                if isinstance(v,(list,dict)):
                    self.printObj(v)
                else: 
                    print text.format(k=k.title(), v=v)
        elif isinstance(obj, list):
            for v in obj:
                if isinstance(v,(list,dict)):
                    self.printObj(v)
                else: 
                    print text.format(k=v,v=v)

    def __repr__(self):
        return "Homestay : {h}\nTicket : {t}\nTransport : {tr}\nTour Guide : {tg}".format(h=self.homestay,t=self.ticket,tr=self.transport,tg=self.tourguide)

